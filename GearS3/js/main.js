
window.onload = function () {
    // add eventListener for tizenhwkey
    document.addEventListener('tizenhwkey', function(e) {
        if(e.keyName === "back") {
			try {
//			    tizen.application.getCurrentApplication().exit();
			} catch (ignore) {
			}
		}
	});
    
    document.addEventListener('visibilitychange', visibilitychange);

    /* Define the event handler in the main.js file */
    function visibilitychange()
    {
    	console.log("visibilitychange = " + document.visibilityState);
    	if (document.visibilityState === 'hidden')
    	{
    	}
    	else
    	{
    	   /* Load stored data and update the page */
    	}
    }
};
