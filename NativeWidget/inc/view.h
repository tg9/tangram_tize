/*
 * Copyright (c) 2015 Samsung Electronics Co., Ltd
 *
 * Licensed under the Flora License, Version 1.1 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://floralicense.org/license/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */ 

#include <Elementary.h>
#include <widget_app.h>

typedef struct widget_instance_data {
	Evas_Object *win;
	Evas_Object *bg;
	Evas_Object *content;

	int root_width;
	int root_height;
} widget_instance_data_s;

/**
 * Identifier of callbacks to be set for Widget.
 *
 * @see view_set_event_callback()
  */
typedef enum _widget_event_type {
   WIDGET_EVENT_PRESSED,   /* Touch Press Event */
   WIDGET_EVENT_RELEASED,  /* Touch Release Event */
   WIDGET_EVENT_CLICKED     /* Touch Click Event */
} widget_event_type;

/* Widget event callback function signature */
typedef void  (*widget_event_cb)(widget_instance_data_s *wid);

void view_set_image(void *user_data, const char *part, const char *file_name);
void view_set_text(void *data, const char *part, const char *text);
widget_instance_data_s *view_create(widget_context_h context, int w, int h);
void view_destroy(void *user_data);
void view_resize(void *user_data, int w, int h);
void view_set_event_callback(void *user_data, const char *part_name, widget_event_type type, void *func);
