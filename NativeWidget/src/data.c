/*
 * Copyright (c) 2015 Samsung Electronics Co., Ltd
 *
 * Licensed under the Flora License, Version 1.1 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://floralicense.org/license/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */ 

#include <Elementary.h>
#include <dlog.h>
#include "log.h"
#include "data.h"

/*
 * @brief: Initialize data that is used in this application.
 */
void data_initialize(void)
{
	/*
	 * If you need to initialize managing data,
	 * please use this function.
	 */
}

/*
 * @brief: Destroy data that is used in this application.
 */
void data_finalize(void)
{
	/*
	 * If you need to finalize managing data,
	 * please use this function.
	 */
}

/*
 * @brief: Get text according to given part
 * @param[part]: name string of one of the existing text parts in the EDJ group
 */
char *data_get_text(const char *part)
{
	/*
	 * You can use this function to retrieve data.
	 */
	char *ret = NULL;

	if (!strcmp("widget.region", part)) {
		ret = strdup("San Francisco");
	} else if (!strcmp("widget.time", part)) {
		ret = strdup("9:30 AM");
	} else if (!strcmp("widget.date", part)) {
		ret = strdup("Wed, Jul 28");
	} else {
		dlog_print(DLOG_ERROR, LOG_TAG, "failed to get text.");
		return NULL;
	}

	return ret;
}

/* End of file */
