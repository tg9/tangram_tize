/*
 *      Copyright (c) 2016 Samsung Electronics Co., Ltd
 *
 *      Licensed under the Flora License, Version 1.1 (the "License");
 *      you may not use this file except in compliance with the License.
 *      You may obtain a copy of the License at
 *
 *              http://floralicense.org/license/
 *
 *      Unless required by applicable law or agreed to in writing, software
 *      distributed under the License is distributed on an "AS IS" BASIS,
 *      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *      See the License for the specific language governing permissions and
 *      limitations under the License.
 */


/*
 * We must declare global variables to let the validator know the
 * global variables we use.
 * To declare global variables, write global variables along with
 * default values inside a comment on top of each source code.
 * The comment should start with the keyword, global.
 */


(function() {
	/**
     * Launch the selected application
     * @param {String} appId - Application name that is included in appIds
     */
//    var launchApp = function(appId) {
//        function onsuccess() {	// Called if the launch is succeeded
//            // Do nothing now
//        }
//        console.log("launchApp appId = " + appId);
//        window.tizen.application.launch(appId, onsuccess);
//    };

    window.onload = function() {
        // add eventListener for tizenhwkey
        document.addEventListener('click|load', function(e) {
            if (e.keyName === 'back') {
                try {
//                    tizen.application.getCurrentApplication().exit();
                } catch (ignore) {
                }
            }
        });
    };
}());
